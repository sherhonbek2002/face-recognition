import cv2 as cv
import os
import numpy as np

people = ['madonna', 'elton_john', 'jerry_seinfeld', 'ben_afflek', 'mindy_kaling']

haar_cascade = cv.CascadeClassifier('haar_face.xml')

features = []
labels = []

DIR = 'data/train'

def create_train():
    for person in people:
        label = people.index(person)
        img_array = os.path.join(DIR,person)
        for img in os.listdir(img_array):
            img_path = os.path.join(img_array,img)
            img = cv.imread(img_path)
            gray = cv.cvtColor(img,cv.COLOR_RGB2GRAY)
            img_rec = haar_cascade.detectMultiScale(gray,scaleFactor=1.1,minNeighbors=3)
            for (x,y,w,h) in img_rec:
                face_roi = gray[x:x+w,y:y+h]
                features.append(face_roi)
                labels.append(label)


create_train()

features = np.array(features, dtype='object')
labels = np.array(labels)

face_recog = cv.face.LBPHFaceRecognizer_create()
face_recog.train(features,labels,)

face_recog.save('face_trained.yml')

np.save('features.npy',features)
np.save('labels.npy',labels)
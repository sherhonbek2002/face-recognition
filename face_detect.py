import cv2 as cv
import numpy as np

features = np.load('features.npy',allow_pickle=True)
labels = np.load('labels.npy',allow_pickle=True)
people = ['madonna', 'elton_john', 'jerry_seinfeld', 'ben_afflek', 'mindy_kaling']
face_reg = cv.face.LBPHFaceRecognizer_create()
face_reg.read('face_trained.yml')
haar_cascade = cv.CascadeClassifier('haar_face.xml')

img = cv.imread('index.jpeg')

gray = cv.cvtColor(img,cv.COLOR_BGR2GRAY)
face_rect = haar_cascade.detectMultiScale(gray,1.1,4)

for (x,y,w,h) in face_rect:
    face_roi = gray[x:x+w,y:y+h]
    label, confidence = face_reg.predict(face_roi)
    cv.rectangle(img,(x,y),(x+w,y+h),(0,255,0),2)
    print(confidence)
    cv.putText(img,str(people[label]),(20,20),cv.FONT_HERSHEY_COMPLEX,1,(0,255,0),2)

cv.imshow('Face',img)

cv.waitKey(0)


